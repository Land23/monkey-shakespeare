from multiprocessing.dummy import freeze_support
import sys
import defs
from numpy.random import randint as rand
from numpy.random import choice as ch
import numpy
from multiprocessing import Process
from multiprocessing import Queue
import string
from time import sleep
import argparse
import os
import time
import colorama
import platform

colorama.init()
f = colorama.Fore

log = defs.log(print, input)

with open("hamlet.txt", "r") as file:
	data = []
	for line in file:
		
		for i in line.split():
			if len(i) >= 5:
				data.append(i.lower())

def monkey(delay, q):
	from textwrap import wrap

	#while True:
	#	try:
	#		if q.get(timeout=5) == "start":
	#			break
	#	except:
	#		pass

	while True:
		n = ch(wrap(string.ascii_lowercase, 1))
		q.put(n)
		sleep(delay/1000)

#@defs.logger
def create_processes(amount: int, func, args: list):
	if amount == 1:
		log.print(f"Starting {amount} process")
	else:
		log.print(f"Starting {amount} processes")

	processes = []
	q = Queue()

	for _ in range(amount):
		log.print(f"Starting process {_}")
		process = Process(target=func, args=args + [q])
		processes.append(process)

	return processes, q

def argparser(args):
	parser = argparse.ArgumentParser(description="Monkey")
	parser.add_argument("-a", "--amount", type=int, help="Amount of processes", default=3)
	parser.add_argument("-d", "--delay", type=int, help="Delay between processes (millaseconds)", default=1)
	return parser.parse_args(args)

def update(s):

	if platform.system() == "Windows":
		os.system("cls")
	else:
		os.system("clear")

	print(s)

#@defs.logger
def main():

	args = argparser(sys.argv[1:])

	am = args.amount

	log.print("Starting main process.")
	processes, q = create_processes(int(am), monkey, [args.delay])

	log.print("Starting monkeys.")
	for process in processes:
		process.start()

	letters = numpy.array([])
	valid_words = numpy.array([])

	lastupdate = time.time()
	update("")
	lastword = ""

	colors = [f.LIGHTBLACK_EX, f.RED, f.LIGHTRED_EX, f.YELLOW, f.GREEN, f.CYAN, f.BLUE, f.MAGENTA, f.LIGHTMAGENTA_EX, f.WHITE]

	while True:
		try:
			s = q.get(timeout=args.delay*5)
		except:
			continue


		letters = numpy.append(letters, str(s))

		last = letters[-20:]
		
		for let in range(len(last)):
			check = last[:let+1]
			if "".join(check) in data:
				valid_words = numpy.append(valid_words, ["".join(check)])
				lastword = "".join(check)

		if time.time() - lastupdate > 1:
			s = "\n".join(letters[-10:])

			s = ""
			let = letters[-10:]
			
			for c in range(len(let)):
				s += f"\n{colors[c]}{let[c]}{f.RESET}"



			st = f"{f.GREEN}Monkey typin{f.RESET}" + "\n"*2
			st += f'''
Last 10 letters:
{s}
'''
			valid_words.sort()
			st += "\n"*2
			if valid_words != numpy.array([]):

				le = [0, ""]
				for word in valid_words:
					if len(word) > le[0]:
						le = [len(word), word]
					elif len(word) == le[0]:
						le.append(word)

				st += f"Longest word(s): {', '.join(le[1:])}\n\n"

				st += "Valid words: " + ", ".join(valid_words) + "\n\n"

				st += f"Last word found: {lastword}\n"
			update(st)

			lastupdate = time.time()




if __name__ == "__main__":
	freeze_support()
	main()


