from colorama import Fore as f
from datetime import datetime
import time
from random import randint as rand
import sys

class log:
	def __init__(self, print, input):

		printf = print
		inputf = input

		def print(msg):
			printf(f"{f.GREEN}[{datetime.now().strftime('%H:%M:%S')}]{f.RESET} {msg}")

		def input(msg):
			return inputf(f"{f.GREEN}[{datetime.now().strftime('%H:%M:%S')}]{f.RESET} {f.LIGHTMAGENTA_EX}{msg}{f.RESET} ")

		def error(msg):
			printf(f"{f.RED}[{datetime.now().strftime('%H:%M:%S')}]{f.RESET} {msg}")
			sys.exit(1)

		def warning(msg):
			printf(f"{f.YELLOW}[{datetime.now().strftime('%H:%M:%S')}]{f.RESET} {msg}")

		def info(msg):
			printf(f"{f.CYAN}[{datetime.now().strftime('%H:%M:%S')}]{f.RESET} {msg}")

		self.print = print
		self.input = input
		self.error = error
		self.warning = warning
		self.info = info

def logger(func):


	def wrapper(*args, **kwargs):
		print(f"{f.GREEN}[{datetime.now().strftime('%H:%M:%S')}]{f.RESET} {f.LIGHTBLUE_EX}{func.__name__}{f.RESET}")
		start = time.time()
		val = func(*args, **kwargs, log=log(print, input))
		end = time.time()
		print(f"{f.GREEN}[{datetime.now().strftime('%H:%M:%S')}]{f.RESET} {f.LIGHTBLUE_EX}{func.__name__}{f.RESET} {f.LIGHTGREEN_EX}Completed in {round(end-start, 2)}s{f.RESET}")
		return val


	return wrapper

def sl():
	time.sleep(rand(3,7)/10)

